from functools import wraps
from time import time
from flask import g, request

import logging
log = logging.getLogger(__name__)

name = "datalogger"
version = "0.2.10"


class DataLogger:
    """
    Basic datalogger dict maintenance class - can be used to aggregate metrics in a specific context
    """

    def __init__(self):
        self._master_log_data = {
            "counts": {},
            "duration": {},
            "tags": []
        }

    def add_count(self, name: str, count: int = 1):
        if name in self._master_log_data['counts'] and self._master_log_data['counts'][name]:
            self._master_log_data['counts'][name] += count
        else:
            self._master_log_data['counts'][name] = count

    def add_duration(self, name: str, count: int):
        self._master_log_data['duration'][name] = count

    def add_dimension(self, name: str, value: str):
        self._master_log_data[name] = value

    def add_tag(self, name: str):
        self._master_log_data['tags'].append(name)

    def clear(self):
        self._master_log_data.clear()

        self._master_log_data['counts'] = {}
        self._master_log_data['duration'] = {}
        self._master_log_data['tags'] = []

    def get_dict(self):
        return self._master_log_data.copy()

    # decorator for any method logging
    def function_logger(self, funct):

        @wraps(funct)
        def wrapper(*args, **kwargs):
            t0 = time()
            response = funct(*args, **kwargs)
            t1 = time()
            time_elapsed = int((t1 - t0) * 1000)

            self.add_dimension('time_elapsed', time_elapsed)
            self.add_dimension('function_name', funct.__name__)

            # log.info(self.get_dict())

            return response

        return wrapper


class FlaskDataLogger:
    """
    Class which can be applied on flask application to push default request dimensions on each application request
    """

    def __init__(self, app, dl: DataLogger):
        if not app:
            raise Exception("application not found")
        self.dl = dl
        app.before_request(self.before_request)
        app.after_request(self.after_request)

    def before_request(self):
        self.dl.clear()
        g.request_start_time = time()
        request_id = getattr(g, 'request_id', 'unknown')
        self.dl.add_dimension("request_id", str(request_id))
        self.dl.add_dimension("request_path", str(request.path))
        self.dl.add_dimension("source_ip", str(request.remote_addr))
        self.dl.add_dimension("user_agent", str(request.headers.get("User-Agent", None)))
        if request.headers.get("x-forwarded-for"):
            self.dl.add_dimension("x-forwarded-for", request.headers.get("x-forwarded-for"))
        if request.headers.get("x-api-key"):
            self.dl.add_dimension("x-api-key", request.headers.get("x-api-key"))

    def after_request(self, response):
        if hasattr(g, 'request_start_time'):
            self.dl.add_duration("request_duration", int((time() - g.request_start_time) * 1000))
        return response


def log_meta(dl: DataLogger, key=None):
    """
    Decorator which can be applied on functions/methods to push respective function metrics to datalogger
    Uses datalogger to construct metrics dict (specific to request)
    """
    def actual_decorator(f):
        identifier = "lm#"+f.__module__+"#"+f.__qualname__ if not key else key

        @wraps(f)
        def wrapped_f(*args, **kwargs):
            t0 = time()
            response = f(*args, **kwargs)
            t1 = time()

            dl.add_duration(identifier, int((t1 - t0) * 1000))
            dl.add_count(identifier)

            return response
        return wrapped_f
    return actual_decorator
