# Datalogger

Log structure: 
```buildoutcfg
{
    "counts": {},
    "duration": {},
    "tags": [],
    "customDimension": "customValue"
}
```

Usage of plain DataLogger in flask route handlers:

```buildoutcfg
from datalogger import DataLogger


@flask_app.before_request
def before_request():
    DataLogger.clear()
    g.request_id = uuid.uuid4()
    DataLogger.add_dimension("request_id", str(g.request_id))
    
@flask_app.after_request
def after_request(response):
    log.info(json.dumps(DataLogger.get_dict()))
    DataLogger.clear()
```




Usage of log_meta (can be applied to functions to log their metrics):
```buildoutcfg
# app.py
from datalogger import DataLogger

data_logger = DataLogger()

```
```buildoutcfg
# decorators.py
from datalogger import log_meta
from app import data_logger

@log_meta(data_logger)
def foo():
    print("In Here")

```

Usage of FlaskDataLogger (can be applied on flask application to log default request dimensions)

```buildoutcfg
# app.py
from datalogger import DataLogger

data_logger = DataLogger()
flask_app = Flask(__name__)

```
```buildoutcfg
# flask_handlers.py

from datalogger import FlaskDataLogger
from app import flask_app

FlaskDataLogger(flask_app, data_logger) # To be applied at the end of all other hook handlers

```

#### Usage of `function_logger` decorator in DataLogger Class

##### For internal functions

Before usage
```
def some_function():
    return pass
```

After usage
```
from datalogger import DataLogger

data_logger = DataLogger()

@data_logger.function_logger
def some_funciton():
return pass
```

##### For third party libraries

Note: This needs an additional file in the project where we need to monitor functions. 
Please make sure you add 'function_monitoring.py' in the src

function_monitoring.py should look like this

```
from datalogger import DataLogger
import some_library

data_logger = DataLogger()

def start_function_logging():
    some_library.some_function = data_logger.function_logger(some_library.some_function)
```

This function `start_function_logging` should be called when the app initializes.
